<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <meta charset="utf-8">
  <title>backend - 3</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
</head>

<body>
    <div class="main">
        <div class="container">
            <div class="window">

                <div class="buttons">
                    <i class="fa fa-circle red-btn"></i>
                    <i class="fa fa-circle yellow-btn"></i>
                    <i class="fa fa-circle green-btn"></i>
                </div>

                <div class="form">
                    <span class="public-class">public class <span class="name">Form </span><span class="fname">{</span></span>
                    <form action="" method="POST">
                        <span class="string">String</span> fullName = {
                        <input type="text" name="fio" placeholder="Name is NULL">
                        };
                      <br><span class="pink">Integer</span> E-mail = {
                        <input type="email" name="email" placeholder="Email is NULL">
                    };
                      <br><span class="pink">Integer</span> yearOfBirth = [
                    <select id="select" name="yob">
                        <option value="1990">1990</option>
                        <option value="1991">1991</option>
                        <option value="1992">1992</option>
                        <option value="1993">1993</option>
                        <option value="1994">1994</option>
                        <option value="1995">1995</option>
                        <option value="1996">1996</option>
                        <option value="1997">1997</option>
                        <option value="1998">1998</option>
                        <option value="1999">1999</option>
                        <option value="2000">2000</option>
                        <option value="2001" selected="selected">2001</option>
                        <option value="2002">2002</option>
                    </select>
                    as <span class="blue">datatime.now()</span>]


                    <div class="genders">
                        <div>
                            <span class="bool">Boolean</span> gender =
                        </div>
                        <div class="form_toggle">
                            <div class="form_toggle-item item-1">
                                <input id="fid-1" type="radio" name="gender" value="M" checked>
                                <label for="fid-1">Man</label>
                            </div>
                            <div class="form_toggle-item item-2">
                                <input id="fid-2" type="radio" name="gender" value="W">
                                <label for="fid-2">Woman</label>
                            </div>
                        </div>
                    </div>
            
                    <br><span class="pink">Integer</span> numberOfLimbs = (( 
                    <input type="radio" name="n_limbs" value="0" />0 ||
                    <input type="radio" name="n_limbs" value="1" />1 ||
                    <input type="radio" name="n_limbs" value="2" />2 ||
                    <input type="radio" name="n_limbs" value="3" />3 ||
                    <input type="radio" checked="checked" name="n_limbs" value="4" />4 ||
                    <input type="radio" name="n_limbs" value="5" />5 ))
                    <div class="ability">
                        <span class="blue">superpowers[] = </span><span>[</span>
                        <select name="sp-sp[]" multiple=multiple>
                            <option value="Immortal">Бессмертие</option>
                            <option value="Through the walls"> Прохождение сквозь стены</option>
                            <option value="Levitation">Левитация</option>
                            <option value="Fireball">Огонь</option>
                        </select>
                        <span>]</span>;
                    </div>
            
                        <div class="biogr">
                            <span><span class="string">String </span>biography = '</span>
                            <textarea placeholder="info is Null" name="bio" rows="3" cols="60"></textarea>'
                        </div>
            
                      <label>
                        <span class="bool">Boolean</span> checkContract =
                        <input type="checkbox" name="check-ok" checked="checked">
                      </label>
                      <br>
                      <div class="constr">
                          Form(<span class="gray">str:</span>fullName, <span class="gray">int:</span>E-mail, <span class="gray">int:</span>yearOfBirth, <span class="gray">bool:</span>gender, <span class="gray">int:</span>numberOfLimbs, <span class="gray">array:</span>superpowers[], <span class="gray">str:</span>biography, <span class="gray">bool:</span>checkContract){
                            <div class="construct">
                                <br><span class='pink'>this.</span><span class="yellow">fullName</span> = fullName;
                                <br><span class='pink'>this.</span><span class="yellow">E-mail</span> = E-mail;
                                <br><span class='pink'>this.</span><span class="yellow">yearOfBirth</span> = yearOfBirth;
                                <br><span class='pink'>this.</span><span class="yellow">gender</span> = gender;
                                <br><span class='pink'>this.</span><span class="yellow">numberOfLimbs</span> = numberOfLimbs;
                                <br><span class='pink'>this.</span><span class="yellow">superpowers</span>=superpowers;
                                <br><span class='pink'>this.</span><span class="yellow">biography</span>=biography;
                                <br><span class='pink'>this.</span><span class="yellow">checkContract</span>=checkContract;
                            </div>
                            <br>
                          }
                        </div>
                    <div class="btn">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <input type="submit" value="submit">
                    </div>
                    </form>
                <span class="public-class">}</span>
                  </div>

            </div>
        </div>
    </div>

    <div class="footer">

    </div>

    <script
  src="https://code.jquery.com/jquery-3.6.0.slim.js"
  integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY="
  crossorigin="anonymous"></script>
  <script src="jquery.maskedinput.js"></script>
    <script src="script.js"></script>
    
</body>

</html>
