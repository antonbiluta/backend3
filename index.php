<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['save'])) {
      print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}
  
  

try{
    
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        print('Заполните имя.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['email'])) {
        print('Заполните почту.<br/>');
        $errors = TRUE;
    }

    if (empty($_POST['bio'])) {
        print('Заполните биографию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['check-ok'])) {
        print('Вы должны быть согласны с условиями.<br/>');
        $errors = TRUE;
    }

    if ($errors) {
        exit();
    }


    $conn = new PDO("mysql:host=localhost;dbname=u20935", 'u20935', '1530928', array(PDO::ATTR_PERSISTENT => true));

    $user = $conn->prepare("INSERT INTO users SET fio = ?, email = ?, yob = ?, gender = ?, n_limbs = ?, bio = ?");
    $user -> execute([$_POST['fio'], $_POST['email'], $_POST['yob'], $_POST['gender'], $_POST['n_limbs'], $_POST['bio']]);
    $id_user = $conn->lastInsertId();

    $abilitys = $conn->prepare("INSERT INTO abilitys SET id_user = ?");
    $abilitys -> execute([$id_user]);
    $id_abil = $conn->lastInsertId();

    $abil = implode(',',$_POST['sp-sp']);

    $ability = $conn->prepare("INSERT INTO ability SET abilitysId = ?, names_ability = ?");
    $ability -> execute([$id_abil, $abil]);

    header("Location: ?save=1");
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
?>
